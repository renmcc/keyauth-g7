package main

import (
	"gitee.com/go-course/keyauth-g7/cmd"
)

func main() {
	cmd.Execute()
}
